import re
import random as rand

def parse_from_gviz(gviz_file):
    with open(gviz_file, "r") as f:
        raw_graph = f.read()
    graph = CFGGraph()
    i = 0
    addr_to_vertex = {} 
    search_hex = lambda s: re.search("0x[0-9a-fA-F]+", s)
    for line in raw_graph.splitlines():
        splitted = line.split()
        first_search = search_hex(splitted[0])
        if first_search is None:
            continue
        vert = first_search.group()
        if splitted[1] != "->":
            addr_to_vertex[vert] = i
            graph.add_vertex(vert)
            graph[i] = []
            i += 1
        else:
            dest_addr = search_hex(splitted[2]).group()
            dest_vert = addr_to_vertex[dest_addr]
            i_vert = addr_to_vertex[vert]
            graph[i_vert].append(dest_vert)

    return graph


class CFGGraph(dict):
    def __init__(self):
        self.vertexes = []
        self.n_paths = None

    def size_vert(self):
        return len(self.vertexes)
    

    def add_vertex(self, v_addr):
        self.vertexes.append(v_addr)
        nv = len(self.vertexes) 
        self[nv-1] = []
    

    def add_edge(self, v1, v2):
        s = self.size_vert()
        if v1 >= s or v2 >= s:
            raise Exception("Vertex not in graph")
        self[v1].append(v2)

    def predecessors(self, vert):
        predecessors = []
        for k, val in self.items():
            if vert in val:
                predecessors.append(k)
        return predecessors

    def walk_with_instrumentation(self, start, instrumentation, path_count):
        '''Esegue una random walk eseguendo la strumentazione
        di ciascun arco percorso'''
        current_node = start
        r = -1
        count = [0 for _ in range(path_count)]
        path = []
        last = self.size_vert()-1
        while current_node != last:
            path.append(current_node)
            nxt = rand.choice(self[current_node])
            edge = (current_node, nxt)
            instr = instrumentation.get(edge)
            if instr is not None:
                kind, val = instr
                if kind == 0:
                    r = val
                elif kind == 1:
                    count[r+val] += 1
                elif kind == 2:
                    count[val] += 1
            current_node = nxt
        path.append(last)
        return count, path



    def get_edge_weights(self):
        '''Algoritmo Fig.5 MICRO96

        per il momento si assume che r2 produca i vertici
        in ordine topologico'''
        weighted_edges = {}
        n_paths = [0 for _ in range(self.size_vert())]
        for i in range(self.size_vert()-1, -1, -1):
            if i == self.size_vert()-1:
                n_paths[i] = 1
            else:
                n_paths[i] = 0
                nxt = self[i]
                for dv in nxt:
                    e = (i, dv)
                    weighted_edges[e] = n_paths[i]
                    n_paths[i] += n_paths[dv]
        self.n_paths = n_paths
        return weighted_edges

