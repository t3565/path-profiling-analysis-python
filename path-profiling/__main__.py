from CFGraph import *
from MSTree import *
from math import inf
from operations import *
import sys
import pydot

argv = sys.argv

pngfname = "a.png"
gviz_file = None

# Gestione informazioni linea di comando
i = 1
while i < len(argv):
    if argv[i] in ("-o", "--output"):
        pngfname = argv[i+1]
        i += 1
    else:
        gviz_file = argv[i]
    i += 1

if gviz_file is None:
    print("You must specify a gviz file to open")
    print("Example:")
    print("python" + argv[0] + " [-o png_out] <gviz_path>")
    sys.exit(1)

if len(argv) == 1:
    print("You need to specify a .gviz file")
    sys.exit(1)

graph = parse_from_gviz(gviz_file) 

dot_graph = pydot.graph_from_dot_file(gviz_file)[0]

weights = graph.get_edge_weights() # corretto
# Arco fittizio richiesto dal paper
graph.add_edge(graph.size_vert()-1, 0)
weights[(graph.size_vert()-1, 0)] = -inf

# Costruisce MST e ottiene le corde
mst, chords = build_mst(graph, weights) # corretto
# Applica i pesi alle corde 
chord_vals = get_chord_weights(mst, chords, weights)
# Somma totale dei pesi sulle corde (ovvero tutti i possibili percorsi)
path_count = sum(map(lambda x: x[1], chord_vals))
# Crea la strumentazione (Algoritmo Fig. 8 MICRO96)
instr = instrument(graph, mst, chords, chord_vals)
# Esegue una random walk a partire dal nodo 0 (inizio) con la strumentazione
# e restituisce il vettore `count` (vedere MICRO96) e il percorso seguito
count_vec, path = graph.walk_with_instrumentation(0, instr, path_count)

#print(dot_graph.get_node_list())
#print(list(map(lambda x: x.get_name(), dot_graph.get_node_list())))

# Rimuove le corde per poi riaggiungerle con i pesi
for chord in chords:
    # converte da intero a stringa (l'indirizzo di partenza del bb)
    # nota: non so se sia un bug o meno, ma il "nome" di un nodo
    # della libreria pydot contiene effettivamente i doppi apici in questo
    # contesto
    src = "\"" + graph.vertexes[chord[0]] + "\""
    dst = "\"" + graph.vertexes[chord[1]] + "\""
    dot_graph.del_edge(src, dst)
    dot_graph.add_edge(pydot.Edge(src, dst, label=chord_vals[chord]))

dot_graph.write_png(pngfname)

print("GRAPH", graph)
print("EDGE WEIGHTS", weights) # Algoritmo Fig. 5 MICRO96
print("MST", mst)
print("CHORDS", chords)
print("CHORD-WEIGHTS:", chord_vals)
print("INSTRUMENTATION", instr)
print("N_PATHS", graph.n_paths)
print()
print("COUNT", count_vec)
print("PATH", path)


