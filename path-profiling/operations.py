
def instrument(graph, mst, chords, chord_weights):
    '''Algoritmo Fig.8 MICRO96

    Instrumentation come segue:
    (0,X) = "r = X"
    (1,Y) = "count[r+Y]++"
    (2,Z) = "count[Y]++"
    '''
    instrumentation = {} # (u,v) => (0|1, ...)
    WS = [0]
    while len(WS) > 0:
        u = WS.pop()
        for v in graph[u]:
            edge = (u,v)
            if edge in chords:
                instrumentation[edge] = (0, chord_weights.get(edge))
            elif len(graph.count_incoming_edges(w)) == 1: 
                WS.append(v)
            else:
                instrumentation[(u,v)] = (0, 0)

    n = graph.size_vert()-1
    WS = [n]
    while len(WS) > 0: 
        v = WS.pop()
        preds = graph.predecessors(v)
        for u in preds:
            edge = u,v
            if edge in chords:
                w = chord_weights[edge]
                if instrumentation.get(edge) == (0, w):
                    instrumentation[edge] = (2, w)
                else:
                    instrumentation[edge] = (1, w)
            elif len(graph[u]) == 1:
                WS.append(u)
            else:
                instrumentation[edge] = (1, 0)
    return instrumentation

        
def get_chord_weights(mst, chords, weights):
    '''Per ciascuna corda (u->v) esegue una dfs alla ricerca del ciclo da v a u
    sommando tutti i pesi incontrati nel percorso
    '''
    chord_w = {}

    def sum_weights(mst, current, end, visited):
        visited[current] = 1
        for nxt in mst[current]:
            w = weights.get((current, nxt))
            if w is None:
                w = weights.get((nxt, current))
            if visited[nxt] == 0:
                res = sum_weights(mst, nxt, end, visited)
                if res >= 0:
                    return res
            elif nxt == end:
                return w
        return -1
            

    for chord in chords:
        u, v = chord
        visited = [0 for _ in mst.items()]
        chord_weight = sum_weights(mst, u, v, visited)
        chord_w[chord] = max(chord_weight,0) + weights.get(chord)
    return chord_w


