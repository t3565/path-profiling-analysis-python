
def build_mst(cfgraph, weighted_edges):
    n = len(cfgraph.vertexes)
    m = MSTree(n)
    chords = []
    sets = [i for i in range(n)]
    edge_items = weighted_edges.items()
    sorted_edges = sorted(edge_items, key=lambda item: item[1])
    for e, w in sorted_edges:
        u, v = e
        if sets[u] != sets[v]:
            m.add_edge(e)
            c = sets[v]
            for x in range(n):
                if sets[x] == c:
                    sets[x] = sets[u]
        else:
            chords.append(e)
    return m, chords


class MSTree(dict):

    def __init__(self, n):
        for i in range(n):
            self[i] = []


    def add_edge(self, edge):
        u,v = edge
        self.get(u).append(v)
        self.get(v).append(u)


