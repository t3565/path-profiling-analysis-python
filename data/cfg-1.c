#include <stdio.h>

int main() {
        unsigned int x = 10;
        scanf("%d", &x);

        if (x > 50) {
                if (x % 2 == 0) {
                        printf("`Curiouser and curiouser!'");
                        printf("cried Alice\n");

                } else {
                        printf("(she was so much surprised\n");

                }
                printf("So you think you're\n");
                printf("changed, do you?\n");
        } else {
                if (x % 4) {
                        printf("that for the moment she quite");
                        printf("forgot how to speak good English\n");
                } else {
                        printf("'now I'm opening out like the largest");
                        printf("telescope that ever was! Good-bye feet!'\n");

                }

                printf("Dear, dear!  How queer");
                printf("everything is to-day!\n");
        }
}
