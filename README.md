# path-profiling-analysis-python

This program is meant to be used to study path profiling algorithm 
described in Ball-Larus paper


# How to run
The `path-profiling` acts as a module, therefore just run 

`python path-profiling [-o filename.png] data/<your_gviz_file_of_choice>`

You can specify a png output file with the `-o` option. If left blank,
the output file will be `a.png`

# Dependencies
- pydot (https://pypi.org/project/pydot/)

